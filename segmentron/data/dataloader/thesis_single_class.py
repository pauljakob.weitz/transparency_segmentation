"""Pascal Transparent Semantic Segmentation Dataset."""
import os
import logging
import torch
import numpy as np

from PIL import Image
from .seg_data_base import SegmentationDataset


class ThesisSingleSegmentation(SegmentationDataset):
    """ADE20K Semantic Segmentation Dataset.

    Parameters
    ----------
    root : string
        Path to ADE20K folder. Default is './datasets/ade'
    split: string
        'train', 'val' or 'test'
    transform : callable, optional
        A function that transforms the image
    Examples
    --------
    >>> from torchvision import transforms
    >>> import torch.utils.data as data
    >>> # Transforms for Normalization
    >>> input_transform = transforms.Compose([
    >>>     transforms.ToTensor(),
    >>>     transforms.Normalize((.485, .456, .406), (.229, .224, .225)),
    >>> ])
    >>> # Create Dataset
    >>> trainset = TransparentSegmentation(split='train', transform=input_transform)
    >>> # Create Training Loader
    >>> train_data = data.DataLoader(
    >>>     trainset, 4, shuffle=True,
    >>>     num_workers=4)
    """
    BASE_DIR = 'thesis_single_class'
    NUM_CLASS = 2

    def __init__(self, root='datasets/transparent', split='test', mode=None, transform=None, testclass=None, **kwargs):
        super(ThesisSingleSegmentation, self).__init__(root, split, mode, transform, **kwargs)
        root = os.path.join(self.root, self.BASE_DIR)
        assert os.path.exists(root), "Please put the data in {SEG_ROOT}/datasets/transparent"
        self.images, self.masks = _get_thesis_pairs(testclass, root, split)
        assert (len(self.images) == len(self.masks))
        if len(self.images) == 0:
            raise RuntimeError("Found 0 images in subfolders of:" + root + "\n")
        logging.info('Found {} images in the folder {}'.format(len(self.images), root))
        self.valid_classes = [0, 1]
        self._key = np.array([0, 1])
        # self._mapping = np.array(range(-1, len(self._key) - 1)).astype('int32')
        self._mapping = np.array(range(-1, len(self._key) - 1)).astype('int32') + 1

    def _mask_transform(self, mask):
        return torch.LongTensor(np.array(mask).astype('int32'))

    def _val_sync_transform_resize(self, img, mask):
        short_size = self.crop_size
        img = img.resize(short_size, Image.BILINEAR)
        mask = mask.resize(short_size, Image.NEAREST)

        # final transform
        img, mask = self._img_transform(img), self._mask_transform(mask)
        return img, mask

    def __getitem__(self, index):
        img = Image.open(self.images[index]).convert('RGB')
        if self.mode == 'test':
            img = self._img_transform(img)
            if self.transform is not None:
                img = self.transform(img)
            return img, os.path.basename(self.images[index])
        mask = Image.open(self.masks[index])
        mask = np.array(mask)[:, :, :3].mean(-1)
        mask[mask == 85.0] = 1
        mask[mask == 170.0] = 1
        mask[mask == 255.0] = 1
        assert mask.max() <= 1, mask.max()
        mask = Image.fromarray(mask)
        # synchrosized transform
        if self.mode == 'train':
            img, mask = self._sync_transform(img, mask, resize=True)
        elif self.mode == 'val':
            img, mask = self._val_sync_transform_resize(img, mask)
        else:
            assert self.mode == 'testval'
            img, mask = self._val_sync_transform_resize(img, mask)
        # general resize, normalize and to Tensor
        if self.transform is not None:
            img = self.transform(img)
        return img, mask, os.path.basename(self.images[index])

    def __len__(self):
        return len(self.images)

    @property
    def pred_offset(self):
        return 1

    @property
    def classes(self):
        """Category names."""
        return ('background', 'transparent')


def _get_thesis_pairs(testclass, folder, mode='train'):
    def get_path_pairs(img_folder, mask_folder):
        img_paths = []
        mask_paths = []
        imgs = os.listdir(img_folder)

        for imgname in imgs:
            imgpath = os.path.join(img_folder, imgname)
            maskname = imgname.replace('.jpg', '.png')
            maskpath = os.path.join(mask_folder, maskname)
            if os.path.isfile(imgpath) and os.path.isfile(maskpath):
                img_paths.append(imgpath)
                mask_paths.append(maskpath)
            else:
                logging.info('cannot find the mask or image:', imgpath, maskpath)

        logging.info('Found {} images in the folder {}'.format(len(img_paths), img_folder))
        return img_paths, mask_paths


    if mode == 'train':
        img_folder = os.path.join(folder, mode, 'images')
        mask_folder = os.path.join(folder, mode, 'masks')
        img_paths, mask_paths = get_path_pairs(img_folder, mask_folder)
    else:
        assert mode == 'val' or mode == 'test'
        clear_img_folder = os.path.join(folder, mode, 'clear', 'images')
        clear_mask_folder = os.path.join(folder, mode, 'clear', 'masks')
        little_img_folder = os.path.join(folder, mode, 'little_dirt', 'images')
        little_mask_folder = os.path.join(folder, mode, 'little_dirt', 'masks')
        much_img_folder = os.path.join(folder, mode, 'much_dirt', 'images')
        much_mask_folder = os.path.join(folder, mode, 'much_dirt', 'masks')
        clear_img_paths, clear_mask_paths = get_path_pairs(clear_img_folder, clear_mask_folder)
        little_img_paths, little_mask_paths = get_path_pairs(little_img_folder, little_mask_folder)
        much_img_paths, much_mask_paths = get_path_pairs(much_img_folder, much_mask_folder)
        if testclass:
            logging.info(f'Testing class {testclass}')
            assert testclass == 'clear' or testclass == 'little_dirt' or testclass == 'much_dirt'
            if testclass == 'clear':
                return clear_img_paths, clear_mask_paths
            if testclass == 'little_dirt':
                return little_img_paths, little_mask_paths
            if testclass == 'much_dirt':
                return much_img_paths, much_mask_paths
        else:
            clear_img_paths.extend(little_img_paths)
            clear_img_paths.extend(much_img_paths)
            clear_mask_paths.extend(little_mask_paths)
            clear_mask_paths.extend(much_mask_paths)
            img_paths = clear_img_paths
            mask_paths = clear_mask_paths
    return img_paths, mask_paths


if __name__ == '__main__':
    train_dataset = ThesisSingleSegmentation()
