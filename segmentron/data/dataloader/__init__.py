"""
This module provides data loaders and transformers for popular vision datasets.
"""
from .mscoco import COCOSegmentation
from .cityscapes import CitySegmentation
from .ade import ADE20KSegmentation
from .pascal_voc import VOCSegmentation
from .pascal_aug import VOCAugSegmentation
from .sbu_shadow import SBUSegmentation
from .transparent11 import TransparentSegmentation
from .transparent11_boundary import TransparentSegmentationBoundary
from .thesis import ThesisSegmentation
from .thesis_demo import DemoSegmentation
from .thesis_single_class import ThesisSingleSegmentation
from .splits import ThesisSplitsSegmentation
from .thesis_single_class_demo import ThesisSingleDemoSegmentation

datasets = {
    'ade20k': ADE20KSegmentation,
    'pascal_voc': VOCSegmentation,
    'pascal_aug': VOCAugSegmentation,
    'coco': COCOSegmentation,
    'cityscape': CitySegmentation,
    'sbu': SBUSegmentation,
    'transparent11': TransparentSegmentation, 
    'transparent11_boundary': TransparentSegmentationBoundary,
    'thesis': ThesisSegmentation,
    'thesis_demo': DemoSegmentation,
    'thesis_single_class': ThesisSingleSegmentation,
    'splits': ThesisSplitsSegmentation,
    'thesis_single_class_demo': ThesisSingleDemoSegmentation
}


def get_segmentation_dataset(name, **kwargs):
    """Segmentation Datasets"""
    return datasets[name.lower()](**kwargs)
