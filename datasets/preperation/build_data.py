import os
import random
import shutil
import re

path = "./labeled/renamed"
folders = ["clear", "little_dirt", "much_dirt"]
splits_root = "./labeled/SPLITS/"
clearimg_root = os.path.join(path, folders[0], "images")


def calc_class():
    seed = random.uniform(0, 1)
    if seed <= 0.8:
        return "train"
    elif seed <= 0.9:
        return "test"
    else:
        return "val"
    


for _ in range(1,6):
    path_result = splits_root + str(_)

    for f in folders:
        os.makedirs(path_result + "/train" + "/images", exist_ok=True)
        os.makedirs(path_result + "/train" + "/masks", exist_ok=True)
        os.makedirs(path_result + "/test/" + f + "/images", exist_ok=True)
        os.makedirs(path_result + "/test/" + f + "/masks", exist_ok=True)
        os.makedirs(path_result + "/val/" + f + "/images", exist_ok=True)
        os.makedirs(path_result + "/val/" + f + "/masks", exist_ok=True)

    for i, _ in enumerate(os.listdir(clearimg_root)):
        data_class = calc_class()
        for f in folders:
            magic_number = folders.index(f)
            directory_mask = os.path.join(path, f, "masks")
            directory_images = os.path.join(path, f, "images")

            dirs = [directory_mask, directory_images]

            for d in dirs:
                #current_dir = os.path.join(path, f, )
                if (data_class == "test" or data_class == "val"):
                    fol = f
                else:
                    fol = ""
                    

                if dirs.index(d) == 0:
                    ext = '.png'
                    newpath = os.path.join(path_result, data_class, fol, "masks", str(i) + '-' + str(magic_number) + ext)
                else:
                    ext = '.jpg'
                    newpath = os.path.join(path_result, data_class, fol, "images", str(i) + '-' + str(magic_number) + ext)
                current_dir = sorted(os.listdir(d))
                
                filepath = os.path.join(d, current_dir[i])
            
                print("Oldname: ", filepath)
                print("Newname: ", newpath)
                
                shutil.copyfile(filepath, newpath)
           


