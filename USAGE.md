Demo:
```
python ./tools/test.py --config-file configs/Thesis/thesis.yaml  DEMO_DIR demo/imgs TEST.TEST_MODEL_PATH workdirs/thesis/4.pth
```

Train:
```
python tools/train.py --config-file configs/Thesis/thesis.yaml --no-cuda
```

Resume training:
```
python tools/train.py --config-file configs/Thesis/thesis.yaml --no-cuda --resume workdirs/thesis/thesis/4.pth
```

Test:
```
python tools/train.py --test --config-file configs/Thesis/thesis.yaml --vis TEST.TEST_MODEL_PATH workdirs/thesis/8.pth
```

Train Single Class:
```
python tools/train.py --config-file configs/Thesis/thesis_single_class.yaml --no-cuda

```

Test Single CLass:
```
python tools/train.py --config-file configs/Thesis/thesis_single_class.yaml --test --vis TEST.TEST_MODEL_PATH .\workdirs\thesis_single_class_1\100.pth TEST.TEST_CLASS much_dirt TEST.SPLIT_ROOT 1
```
